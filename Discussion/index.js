//alert("Hello World");

//REPETITION CONTROL STRUCTURES

//WHILE LOOP
/*
	- a while loop takes in an express/condition
	- If the condition evaluates to be true,  the statements inside the code will be executed

	Syntax:
	while(expression/condition) {
		statement
	}
*/

let  count = 5;

//While the value count is not equal to zero, it will run
while(count !== 0) {

	//The current value of count is printed out
	console.log("While: " + count);

	//Decreases the value of count by 1 after very iteration
	count--;
	//careful with ++
};



//DO WHILE LOOP
/*
	- A do while loop works a lot like the while loop but it guarantees that the code will be executed atleast once.
	Syntax:
	do {
		statement
	}	while(expression/condition)
*/

let number = Number(prompt("Give me a number"));

do {
	// the current value of number is printed out
	console.log("Do While " + number);

	//Increases the value of number by 1 after every iteration = number + 1
	number += 1;

//Providing a number of 10 or greater that will the code block once but the loop will stop there.
} while (number < 10);


//FOR LOOP

/*
	- A for loop is more flexible that while and do-while loops
	- It consist of 3 parts
	1. The "initial" value which is the starting value. The count will start at the initial value
	2. The "expression/condition" will determine if the loop will run one more time. While this is true,  the loop will run
	3. The "finalExpression" which determines how the loop will run (increment/decrement)

	Syntax:
	for(initial, expression/condition, final Expression){
	statement
	}
*/
/*
	- This loop starts from 0
	- This loop while run as long as the value of count is less than or equal to zero
	- this loop adds 1 after every run

// */
// for (let count= 0; count <= 20; count++) {
// 	console.log(count);
// }

// //Characters in String may be counted using the .length property

// //Strings are special compare to ther data types as it is has access to functions and other pieces of information. Other primitive data doesn't have this.

// let myString = "jeru"
// console.log(myString.length)

// //Accessing elements of a string
// //Individual characters of a string may be accessed using its index number
// //The first charcter is a string has an index of "0". The next is one and so on.
// console.log(myString[0])
// console.log(myString[1])
// console.log(myString[2])
// console.log(myString[3])

// //This loop prints the individual letters of myString variable.
// for (let x = 0; x < myString.length; x++) {
// 	//The current value of myString
// 	console.log(myString[x])
// }



/*
	- this loop will print out the letters of the name individually but will print 3 if the letter is a vowel.
	- this loop starts with the value of zero assign to i
	this loop will run as long as the value of i is less than the length of the name 
	this loop increments
*/
let myName = "johndaniel";

for(let i = 0; i < myName.length; i++) {

	// If the character of your name is a vowel letter, instead of displaying the character, it will display the number 3
	// The "toLowertCase" function will change the current letter being

	if (
		myName[i].toLowerCase() == "a"  ||
		myName[i].toLowerCase() == "e"  ||
		myName[i].toLowerCase() == "i"  ||
		myName[i].toLowerCase() == "o"  ||
		myName[i].toLowerCase() == "u"  
		) {
		//print 3 if the condition is met
		console.log(3);
	} else {
		//print the non vowel characters
		console.log(myName[i]);
	}
}

//CONTINUE AND BREAK
/*
	- The "continue" statement allows the code to go to the next iteration of a loop without finishing the execution of a statements is a code block.
	- The "break" statement is used to terminate the loop once a match has been found
*/

for (let count = 0; count <= 20; count++) {

	//If remainder is equal to 0
	if (count % 2 === 0) {
		//Tells the code to continue to the next iteration of the loop
		//Ignores all the statements located after the continue statement
		continue;
	}

	//The current value of number is printed out if the remainder is not equal to 0.

	console.log("Continue and Break: "+ count)
	if (count > 10) {
		//Tells the code to terminate/stop the loop even if the condition/expression defines that it should execute so long as the value of count is less  tan or equal to 20.
		break;
	}
}

let name = "jakedlexter";

for (let i = 0; i < name.length; i++) {
	// the current letter is printed out base on its index
	console.log(name[i]);

	// if the value is a, continue to the next iteration of the loop
	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the next iteration");
		continue;
	}

	//If the current letter is equal to d, stop the loop
	if (name[i] == "d") {
		break;
	}
}
